#include <iostream>

#include "armada/util/console.h"

const std::string MODULE_NAME = "Server";

int main()
{
	std::cout << armada::console::message(MODULE_NAME) << "Hello, World!" <<
		std::endl;
	
	return 0;
}
