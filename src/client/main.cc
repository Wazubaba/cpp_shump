// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <iostream>


#include "armada/util/console.h"
#include "armada/core/tree.h"
#include "armada/core/entity.h"
#include "armada/core/entpool.h"

#include "armada/core/state.h"

#include "armada/controllers/simple.h"

const std::string MODULE_NAME = "Main";

int main(void)
{
	armada::GameState_t engine;
	if (!engine.init())
	{
		std::cerr << "Cannot continue. Shutting down" << std::endl;
		return -1;
	}

	std::cout << armada::console::message(MODULE_NAME) << "Creating window: ";
	sf::RenderWindow* window;

	window = new sf::RenderWindow(sf::VideoMode(800, 600), "shump");
	if (!window)
	{
		std::cout << "Failed!" << std::endl;
		std::cerr << armada::console::error(MODULE_NAME) << "ERROR log here (someday)!" << std::endl;
		return -1;
	}
	std::cout << " Done" << std::endl;

	std::cout << armada::console::notice(MODULE_NAME) << "Creating test entities: ";

	for (size_t idx = 0; idx < 5; idx++)
	{
		auto entity = engine.scene.unit_pool.request_slot();
		entity->init(engine.resources.textures["test"]);
		entity->setPosition((idx * 32) + 16, 0);
		entity->controller = new armada::controller::SimpleController_t();
	}

	std::cout << "Done" << std::endl;

	sf::Event event;
	while (window->isOpen())
	{
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
				case sf::Event::Closed:
					window->close();
					break;

				case sf::Event::KeyPressed:
					switch (event.key.code)
					{
						case sf::Keyboard::Escape:
							window->close();
							goto shutdown;
							break;

						case sf::Keyboard::Space:
						{
							auto entity = engine.scene.unit_pool.request_slot();
							entity->init(engine.resources.textures["test"]);
							sf::Vector2f mpos = (sf::Vector2f) sf::Mouse::getPosition(*window);
							entity->setPosition(mpos);

							std::cout << armada::console::notice(MODULE_NAME) << "Created entity at (" << mpos.x << "," << mpos.y << ")" << std::endl;
							break;
						}

						default:
							break;
					}
					break;

				default:
					break;
			}
		}

		window->clear(sf::Color::Black);
		engine.scene.update(window);
		window->display();
	}

shutdown:
	engine.deinit();
	delete window;
	return 0;
}
