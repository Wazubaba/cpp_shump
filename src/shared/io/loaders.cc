// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/io/loaders.h"
#include "armada/io/database.h"
#include "armada/core/exceptions.h"

#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


/* General loaders */
void armada::io::load_texture(armada::io::ResourceDatabase_t* database, const std::string& id, const std::string path)
{
	database->textures[id] = new sf::Texture();
	database->textures[id]->loadFromFile(path);
	std::cout << "Loaded " << path << std::endl;
}

void armada::io::load_sound(armada::io::ResourceDatabase_t* database, const std::string& id, const std::string path)
{
	database->sounds[id] = new sf::SoundBuffer();
	database->sounds[id]->loadFromFile(path);
	std::cout << "Loaded " << path << std::endl;
}

void armada::io::load_music(armada::io::ResourceDatabase_t* database, const std::string& id, const std::string path)
{
	database->music[id] = new sf::Music();
	database->music[id]->openFromFile(path);
	std::cout << "Loaded " << path << std::endl;
}


/* Json-style loaders */
void armada::io::load_texture (armada::io::ResourceDatabase_t* database, const Json::Value& data)
{
	Json::Value id = data["id"];
	Json::Value path = data["path"];

	if (!id && path)
		throw armada::exception::ResourceLoad("texture", "", path.asString());
	else
	if (id && !path)
		throw armada::exception::ResourceLoad("texture", id.asString(), "");
	else
	if (!id && !path)
		throw armada::exception::ResourceLoad("texture");
	
	database->textures[id.asString()] = new sf::Texture();
	database->textures[id.asString()]->loadFromFile(path.asString());
	std::cout << "\tLoaded " << path << std::endl;
}

void armada::io::load_sound (armada::io::ResourceDatabase_t* database, const Json::Value& data)
{
	Json::Value id = data["id"];
	Json::Value path = data["path"];

	if (!id && path)
		throw armada::exception::ResourceLoad("texture", "", path.asString());
	else
	if (id && !path)
		throw armada::exception::ResourceLoad("texture", id.asString(), "");
	else
	if (!id && !path)
		throw armada::exception::ResourceLoad("texture");
	
	database->sounds[id.asString()] = new sf::SoundBuffer();
	database->sounds[id.asString()]->loadFromFile(path.asString());
	std::cout << "\tLoaded " << path << std::endl;
}

void armada::io::load_music (armada::io::ResourceDatabase_t* database, const Json::Value& data)
{
	Json::Value id = data["id"];
	Json::Value path = data["path"];

	if (!id && path)
		throw armada::exception::ResourceLoad("texture", "", path.asString());
	else
	if (id && !path)
		throw armada::exception::ResourceLoad("texture", id.asString(), "");
	else
	if (!id && !path)
		throw armada::exception::ResourceLoad("texture");
	
	database->music[id.asString()] = new sf::Music();
	database->music[id.asString()]->openFromFile(path.asString());
	std::cout << "\tLoaded " << path << std::endl;
}
