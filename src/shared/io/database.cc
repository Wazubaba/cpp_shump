// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/io/database.h"

#include <iostream>
#include <fstream>

#include <json/json.h>

#include "armada/util/console.h"
#include "armada/core/exceptions.h"
#include "armada/io/loaders.h"


const std::string MODULE_NAME = "Database";

const bool armada::io::ResourceDatabase_t::init()
{
	return load_config("res/cfg/init.json");
}


void armada::io::ResourceDatabase_t::deinit()
{
	/* Delete all textures */
	std::cout << armada::console::notice(MODULE_NAME) <<
		"Unloading textures: ";
	for (auto texture_ref = textures.begin(); texture_ref != textures.end(); ++texture_ref)
		delete texture_ref->second;

	std::cout << "done" << std::endl;

	std::cout << armada::console::notice(MODULE_NAME) <<
		"Unloading all sounds: ";
	for (auto sound_ref = sounds.begin(); sound_ref != sounds.end(); ++sound_ref)
		delete sound_ref->second;

	std::cout << "done" << std::endl;

	std::cout << armada::console::notice(MODULE_NAME) <<
		"Unloading all music: ";
	for (auto music_ref = music.begin(); music_ref != music.end(); ++music_ref)
		delete music_ref->second;
		
	std::cout << "done" << std::endl;
}


const bool armada::io::ResourceDatabase_t::load_config(const std::string& path)
{
	Json::Value root;
	
	std::ifstream file(path);
	file >> root;

	Json::Value textures = root["textures"];
	std::cout << armada::console::notice(MODULE_NAME) <<
		"Loading definitions from " << path << std::endl;
	
	try
	{
		if (textures)
		{
			std::cout << armada::console::message(MODULE_NAME) <<
				"Loading textures:" << std::endl;
		
			for (int idx = 0; idx < textures.size(); idx++)
				armada::io::load_texture(this, textures[idx]);
		}

		Json::Value sounds = root["sounds"];
		if (sounds)
		{
			std::cout << armada::console::message(MODULE_NAME) <<
				"Loading sounds:" << std::endl;
			
			for (int idx = 0; idx < sounds.size(); idx++)
				armada::io::load_sound(this, sounds[idx]);
		}

		Json::Value music = root["sounds"];
		if (music)
		{
			std::cout << armada::console::message(MODULE_NAME) <<
				"Loading music:" << std::endl;
			
			for (int idx = 0; idx < music.size(); idx++)
				armada::io::load_music(this, music[idx]);
		}
	}
	catch (armada::exception::ResourceLoad& err)
	{
		std::cerr << armada::console::error(MODULE_NAME) <<
			"Failed to load config " << path << ": " << err.what() <<
			std::endl;
			return false;
	}

	return true;
}
