// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/core/exceptions.h"

#include <iostream>
#include <sstream>

armada::exception::ResourceLoad::ResourceLoad(const std::string& type, const std::string& id, const std::string& path)
: _type(type), _id(id), _path(path)
{}

const char* armada::exception::ResourceLoad::what() const throw()
{
	std::string message = "Failed to load " + _type + " resource file with ";
	if (_id != "")
		return (message + "id of " + _id).c_str();
	else
	if (_path != "")
		return(message + "path of " + _path).c_str();
	
	else
		return ("Completely failed to load a " + _type + " resource, something is wrong with your configuration.").c_str();
}
