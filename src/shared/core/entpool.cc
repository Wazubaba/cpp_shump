// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>

#include "armada/core/entpool.h"
#include "armada/util/console.h"

const std::string MODULE_NAME = "Entity Pool";
armada::core::Entity_t armada::Entpool_t::pool[armada::Entpool_t::_MAX_POOL];
size_t armada::Entpool_t::active = 0;

void armada::Entpool_t::init()
{
	for (size_t itr = 0; itr < _MAX_POOL; itr++)
		pool[itr].init();

		std::cout << armada::console::notice(MODULE_NAME) <<
			"Allocated space for " << _MAX_POOL << " units" <<
			std::endl;
}

void armada::Entpool_t::deinit()
{
	for (size_t itr = 0; itr < _MAX_POOL; itr++)
		pool[itr].deinit();
	
	std::cout << armada::console::notice(MODULE_NAME) <<
		"Cleanup complete" << std::endl;
}

void armada::Entpool_t::release(core::Entity_t* entity)
{
	for (size_t itr = 0; itr < _MAX_POOL; itr++)
		if (pool[itr].id == entity->id)
		{
			pool[itr].allocated = false;
			active--;
			break;
		}
}

armada::core::Entity_t* armada::Entpool_t::request_slot()
{
	for (size_t itr = 0; itr < _MAX_POOL; itr++)
		if (!pool[itr].allocated)
		{
			pool[itr].allocated = true;
			active++;
			return &pool[itr];
		}
	
	std::cerr << armada::console::error(MODULE_NAME) <<
		"Unable to aquire unit!" << std::endl;
	return nullptr;
}
