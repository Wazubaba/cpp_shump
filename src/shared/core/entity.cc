// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/core/entity.h"

#include <cassert>

#include "armada/util/console.h"
#include "armada/util/math.h"

const std::string MODULE_NAME = "Unit";
int armada::core::Entity_t::_count = 1;

void armada::core::Entity_t::init()
{
	assert (id == 0);
	id = _count++;
	_sprite = new sf::Sprite();
	controller = nullptr;
}

void armada::core::Entity_t::init(sf::Texture* texture)
{
	if (id == 0)
		this->init();
	_sprite->setTexture(*texture);
	controller = nullptr;
}

void armada::core::Entity_t::deinit()
{
	delete _sprite;
	if (controller != nullptr)
		delete controller;
}

void armada::core::Entity_t::update(const sf::Time& dt)
{
	controller->process(dt, this);
}

int armada::core::Entity_t::get_health()
{
	return _health;
}

int armada::core::Entity_t::get_team()
{
	return _team;
}

int armada::core::Entity_t::get_threat_level()
{
	return _threat_level;
}

void armada::core::Entity_t::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= this->getTransform();
	target.draw(*_sprite, states);
}
