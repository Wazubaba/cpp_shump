// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/core/tree.h"
#include "armada/util/console.h"

const std::string MODULE_NAME = "Tree";

void armada::SceneTree_t::init()
{
	unit_pool.init();
	std::cout << armada::console::notice(MODULE_NAME) <<
	"initialized" << std::endl;
}


void armada::SceneTree_t::deinit()
{
	unit_pool.deinit();
	std::cout << armada::console::notice(MODULE_NAME) <<
	"cleanup complete!" << std::endl;
}

void armada::SceneTree_t::update(sf::RenderWindow* window)
{
	sf::Time dt = delta_timer.restart();

	for (size_t itr = 0; itr < unit_pool.active; itr++)
	{
		window->draw(unit_pool.pool[itr]);
		unit_pool.pool[itr].update(dt);
	}
}
