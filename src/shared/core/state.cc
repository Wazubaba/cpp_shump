// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/core/state.h"

#include <iostream>

#include "armada/util/console.h"
#include "armada/core/exceptions.h"

const std::string MODULE_NAME = "GameState";

bool armada::GameState_t::initialized = false;
armada::SceneTree_t armada::GameState_t::scene;
armada::io::ResourceDatabase_t armada::GameState_t::resources;

bool armada::GameState_t::init()
{
	if (!initialized)
	{
		if (!resources.init())
		{
			resources.deinit();
			return false;
		}
		scene.init();
		initialized = true;
	}
	return true;
}

void armada::GameState_t::deinit()
{
	if (initialized)
	{
		resources.deinit();
		scene.deinit();
		initialized = false;
	}
	else
		std::cout << armada::console::warning(MODULE_NAME) <<
			"GameState_t singleton deinit called before initialization!"
			<< std::endl;
}
