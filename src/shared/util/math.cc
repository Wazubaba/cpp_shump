// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/util/math.h"
#define _USE_MATH_DEFINES
#include <cmath>

namespace armada
{
namespace math
{

sf::Vector2f normalize(const sf::Vector2f& a)
{
	int quotient = sqrt(pow(a.x, 2) + pow(a.y, 2));
	return sf::Vector2f(a.x / quotient, a.y / quotient);
}

sf::Vector2f normalize(const sf::Vector2i& a)
{
	int quotient = sqrt(pow(a.x, 2) + pow(a.y, 2));
	return sf::Vector2f(a.x / quotient, a.y / quotient);
}


sf::Vector2f get_distance(const sf::Vector2f& a, const sf::Vector2f& b)
{
	return a - b;
}

sf::Vector2f multiply(const sf::Vector2f& a, const sf::Int64 b)
{
	return sf::Vector2f(a.x * b, a.y * b);
}

sf::Vector2f multiply(const sf::Vector2i& a, const sf::Int64 b)
{
	return sf::Vector2f(a.x * b, a.y * b);
}


float get_angle(const sf::Vector2f& a, const sf::Vector2f& b)
{
	float angle = atan2(
			a.y - b.y,
			a.x - b.x);
	return angle * 180 / M_PI;
}

}
}
