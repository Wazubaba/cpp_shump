#include "armada/util/console.h"
#include <sstream>
#include <ctime>

namespace armada {
namespace console {

#ifndef WINDOWS
#define RED "\e[0;31m"
#define BRED "\e[1;31m"
#define GREEN "\e[0;32m"
#define BGREEN "\e[1;32m"
#define YELLOW "\e[0;33m"
#define BYELLOW "\e[1;33m"
#define BLUE "\e[0;34m"
#define BBLUE "\e[1;34m"
#define MAGENTA "\e[0;35m"
#define BMAGENTA "\e[1;35m"
#define CYAN "\e[0;36m"
#define BCYAN "\e[1;36m"
#define WHITE "\e[0;37m"
#define BWHITE "\e[1;37m"
#define BLACK "\e[0;38m"
#define BBLACK "\e[1;38m"
#define NC "\e[0m"
#else
#define RED ""
#define BRED ""
#define GREEN ""
#define BGREEN ""
#define YELLOW ""
#define BYELLOW ""
#define BLUE ""
#define BBLUE ""
#define MAGENTA ""
#define BMAGENTA ""
#define CYAN ""
#define BCYAN ""
#define WHITE ""
#define BWHITE ""
#define BLACK ""
#define BBLACK ""
#define NC ""
#endif

std::string _get_timestamp()
{
	std::time_t t = std::time(0);
	std::tm* now = std::localtime(&t);

	std::stringstream ss;
	ss << WHITE << "(" << BGREEN << now->tm_year + 1900 << WHITE << "." <<
		BGREEN << now->tm_mon << WHITE << "." << BGREEN <<
		now->tm_mday << WHITE << " - " << BYELLOW << now->tm_hour <<
		WHITE << ":" << BYELLOW << now->tm_min << WHITE << ":" << BYELLOW
		<< now->tm_sec << WHITE << ")";

	return ss.str();
}



	std::string message(const std::string& module)
	{
		std::stringstream ss;
		ss << NC <<  _get_timestamp() << WHITE << " [" << BMAGENTA <<
			module << WHITE << "]" << BCYAN << ": " << WHITE;
		
		return ss.str();
	}

	std::string warning(const std::string& module)
	{
		std::stringstream ss;
		ss << NC << _get_timestamp() << WHITE << " [" << BYELLOW <<
			module << WHITE << "]" << BCYAN << ": " << WHITE;
		
		return ss.str();
	}

	std::string error(const std::string& module)
	{
		std::stringstream ss;
		ss << NC << _get_timestamp() << WHITE << " [" << BRED <<
			module << WHITE << "]" << BCYAN << ": " << WHITE;
		
		return ss.str();
}

	std::string notice(const std::string& module)
	{
		std::stringstream ss;
		ss << NC <<  _get_timestamp() << WHITE << " [" << BMAGENTA <<
			module << WHITE << "]" << BCYAN << ": " << WHITE;
		
		return ss.str();
	}
}

}
