// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/audio/core.h"

#include <cassert>

namespace armada {
namespace audio {

AudioEvent_t AudioEngine_t::_pending[AudioEngine_t::_MAX_PENDING];
int AudioEngine_t::_num_pending;

void AudioEngine_t::init()
{
	_num_pending = 0;
}

void AudioEngine_t::playSound(SoundID_t id, int volume)
{
	assert(_num_pending < _MAX_PENDING);

	_pending[_num_pending].id = id;
	_pending[_num_pending].volume = volume;

	_num_pending++;
}

void AudioEngine_t::update()
{
	for (int idx = 0; idx < _num_pending; idx++)
	{
		/* TODO: Play sounds here */
	}

	_num_pending = 0;
}

}}
