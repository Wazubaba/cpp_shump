// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#include "armada/controllers/simple.h"

#include <iostream>

#include "armada/core/entity.h"
#include "armada/util/console.h"
#include "armada/core/state.h"

void armada::controller::SimpleController_t::process(sf::Time dt, armada::core::Entity_t* entity)
{
	armada::GameState_t state;
	//std::cout << "found " << state.scene.unit_pool.active << " units!"
	//	<< std::endl;
//	std::cout << "test successful!" << std::endl;
}
