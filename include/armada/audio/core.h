// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CORE_H_JRJG6Z4A
#define CORE_H_JRJG6Z4A

#include "events.h"

namespace armada {
namespace audio {

class AudioEngine_t
{
public:
	static void init();
	static void playSound(SoundID_t id, int volume);
	static void update();

private:
	static const int _MAX_PENDING = 16;
	static AudioEvent_t _pending[_MAX_PENDING];
	static int _num_pending;
};

}}
#endif /* end of include guard: CORE_H_JRJG6Z4A */
