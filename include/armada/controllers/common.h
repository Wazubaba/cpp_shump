// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef COMMON_H_KBQDO0Y4
#define COMMON_H_KBQDO0Y4

#include <SFML/Graphics.hpp>

namespace armada {
namespace core {class Entity_t;}
namespace controller {

/*
	Specifies a simple template for all other controllers to adhere to
*/
class iBaseController
{
public:
	virtual void process(sf::Time dt, armada::core::Entity_t* unit);
};

}}
#endif /* end of include guard: COMMON_H_KBQDO0Y4 */
