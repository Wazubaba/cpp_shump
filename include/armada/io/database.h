// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DATABASE_H_YHOJBTFR
#define DATABASE_H_YHOJBTFR

#include <map>
#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

namespace armada {
namespace io {

struct ResourceDatabase_t
{
	std::map<std::string, sf::Texture*> textures;
	std::map<std::string, sf::Music*> music;
	std::map<std::string, sf::SoundBuffer*> sounds;

	const bool init();
	void deinit();

	const bool load_config(const std::string& path);
};

}}
#endif /* end of include guard: DATABASE_H_YHOJBTFR */
