// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LOADERS_H_XBG8CAUK
#define LOADERS_H_XBG8CAUK

#include <string>

#include <json/json.h>

namespace armada {
namespace io {

/* Forward declare */
struct ResourceDatabase_t;

void load_texture(armada::io::ResourceDatabase_t* database, const std::string& id, const std::string path);
void load_texture(armada::io::ResourceDatabase_t* database, const Json::Value& data);

void load_sound(armada::io::ResourceDatabase_t* database, const std::string& id, const std::string path);
void load_sound(armada::io::ResourceDatabase_t* database, const Json::Value& data);

void load_music(armada::io::ResourceDatabase_t* database, const std::string& id, const std::string path);
void load_music(armada::io::ResourceDatabase_t* database, const Json::Value& data);

}}
#endif /* end of include guard: LOADERS_H_XBG8CAUK */
