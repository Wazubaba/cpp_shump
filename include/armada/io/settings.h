#ifndef SETTINGS_H_ZYRKEFFY
#define SETTINGS_H_ZYRKEFFY

#include <SFML/Graphics.hpp>

const int MOVE_UP = sf::Keyboard::W;
const int MOVE_DOWN = sf::Keyboard::S;
const int MOVE_LEFT = sf::Keyboard::A;
const int MOVE_RIGHT = sf::Keyboard::D;

#endif /* end of include guard: SETTINGS_H_ZYRKEFFY */
