// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ENTPOOL_H_KWSNCL4D
#define ENTPOOL_H_KWSNCL4D

#include <cstddef>

#include "armada/core/entity.h"

namespace armada 
{
class Entpool_t
{
private:
	static const size_t _MAX_POOL = 1000;

public:
	static size_t active;

	static void init();
	static void deinit();
	static core::Entity_t* request_slot();
	static void release(core::Entity_t* unit);
	static core::Entity_t pool[_MAX_POOL];
};

}
#endif /* end of include guard: ENTPOOL_H_KWSNCL4D */

