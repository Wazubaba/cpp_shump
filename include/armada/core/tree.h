// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TREE_H_HNUTDKPR
#define TREE_H_HNUTDKPR

#include <iostream>

#include "armada/core/entpool.h"
#include "armada/core/entity.h"
#include "armada/core/entpool.h"

namespace armada {

/*
	this can probably be made non-static now that it is held
	in GameState_t as static.
*/

class SceneTree_t
{
public:
	sf::Clock delta_timer;
	sf::Clock fps_timer;
	Entpool_t unit_pool;

	void init();
	void deinit();

	void update(sf::RenderWindow* window);
};

}
#endif /* end of include guard: TREE_H_HNUTDKPR */
