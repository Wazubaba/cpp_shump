// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UNIT_H_34ITH5H
#define UNIT_H_34ITH5H

#include <iostream>

#include <SFML/Graphics.hpp>

#include "armada/controllers/common.h"

namespace armada {
namespace core {

class Entity_t : public sf::Drawable, public sf::Transformable
{
public:
	void init();
	void init(sf::Texture* texture);
	void deinit();

	bool allocated;
	int id;
	armada::controller::iBaseController* controller;

	int get_health();
	int get_team();
	int get_threat_level();

	void update(const sf::Time& dt);

protected:
	sf::Sprite* _sprite;

private:
	static int _count;
	int _health;
	int _team;
	int _threat_level;
	bool _has_target;
	float _move_speed;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

}}
#endif /* end of include guard: UNIT_H_34ITH5H */
