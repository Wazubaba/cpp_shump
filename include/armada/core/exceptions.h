// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXCEPTIONS_H_J0BTRH64
#define EXCEPTIONS_H_J0BTRH64

#include <string>
#include <exception>

namespace armada {
namespace exception {

class ResourceLoad : public std::exception
{
public:
	ResourceLoad(const std::string& type, const std::string& id = "", const std::string& path = "");
	virtual const char* what() const throw();

private:
	std::string _type;
	std::string _id;
	std::string _path;
};

}}
#endif /* end of include guard: EXCEPTIONS_H_J0BTRH64 */
