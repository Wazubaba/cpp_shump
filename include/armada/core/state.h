// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STATE_H_6IZSCKM9
#define STATE_H_6IZSCKM9

/*
	A global state struct

	This will contain all of the various threads and loaded data.
*/

#include "armada/core/tree.h"
#include "armada/io/database.h"

namespace armada {

struct GameState_t
{
	static armada::SceneTree_t scene;
	static armada::io::ResourceDatabase_t resources;
	static bool initialized;

	bool init();
	void deinit();
};

}
#endif /* end of include guard: STATE_H_6IZSCKM9 */
