// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MAIN_H_CQWOVXPB
#define MAIN_H_CQWOVXPB

#include <SFML/Graphics.hpp>

namespace armada
{
namespace math
{
/* Vector stuff */
sf::Vector2f normalize(const sf::Vector2f& a);
sf::Vector2f normalize(const sf::Vector2i& a);
sf::Vector2f get_distance(const sf::Vector2f& a, const sf::Vector2f& b);


sf::Vector2f multiply(const sf::Vector2f& a, const sf::Int64 b);
sf::Vector2f multiply(const sf::Vector2i& a, const sf::Int64 b);

/* Angle stuff */
float get_angle(const sf::Vector2f& a, const sf::Vector2f& b);

}
}


#endif /* end of include guard: MAIN_H_CQWOVXPB */

