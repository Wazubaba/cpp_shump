// This file is a part of Armada.
//
// Armada is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Armada is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Armada.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CONSOLE_H_3GJSK2WQ
#define CONSOLE_H_3GJSK2WQ

#include <string>

namespace armada
{
	namespace console
	{
		std::string message(const std::string& module);
		std::string warning(const std::string& module);
		std::string error(const std::string& module);
		std::string notice(const std::string& module);
	}
}

#endif /* end of include guard: CONSOLE_H_3GJSK2WQ */
